FROM docker:stable

ENV AWS_CLI_VERSION="< 3.0"
ENV KUBECTL_VERSION=1.31.2

RUN apk add --no-cache \
        ca-certificates \
        gettext \
        py3-pip && \
    pip3 install --no-cache-dir --disable-pip-version-check "awscli $AWS_CLI_VERSION" && \
    wget -q -O /usr/sbin/kubectl "https://dl.k8s.io/release/v1.31.0/bin/linux/amd64/kubectl" && \
    chmod -v 755 /usr/sbin/kubectl && \

COPY kube-config.tpl /etc/kube-config.tpl
COPY prepare-ci.sh /usr/sbin/prepare-ci

RUN chmod +x /usr/sbin/prepare-ci

CMD ["prepare-ci"]
