apiVersion: v1
kind: Config
preferences: {}

clusters:
- name: k8s-cluster
  cluster:
    insecure-skip-tls-verify: true
    server: ${K8S_CLUSTER_SERVER}

contexts:
- name: k8s-deploy
  context:
    cluster: k8s-cluster
    user: deploy

current-context: k8s-deploy

users:
- name: deploy
  user:
    username: ${K8S_DEPLOY_USER}
    client-certificate-data: ${K8S_CLIENT_CRT}
    client-key-data: ${K8S_CLIENT_KEY}
