#!/bin/sh
#
# prepare-ci.sh - CI/CD bootstrapping script
#
# See function 'check_required_vars()' for the required environment variables.
#
##

set -e

# Environment variables

: ${K8S_DEPLOY_USER:=ci-deploys}

export K8S_DEPLOY_USER

# Functions

check_required_vars()
{
    local var

    echo -n "Checking required environment variables... "

    for var in \
        K8S_CLUSTER_SERVER \
        K8S_CLIENT_CRT \
        K8S_CLIENT_KEY
    do
        if [ -z "$( eval "echo \$$var" )" ] ; then
            echo "FATAL: variable $var is not set. Aborting." >&2
            exit 1
        fi
    done

    echo "OK"
}

configure_kubectl()
{
    local kubeconfig="$HOME/.kube/config"

    echo -n "Creating kube config file '$kubeconfig'... "

    mkdir -p "$HOME/.kube"
    cat /etc/kube-config.tpl | envsubst > "$kubeconfig"

    echo "OK"
}

# Entrypoint

main()
{
    # Check
    check_required_vars

    # Configure
    set -x
    configure_kubectl
}

main